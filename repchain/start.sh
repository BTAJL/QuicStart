#!/bin/sh

app_name="RepChain.jar"

#::=== logger
logger_name="logback"

#::=== config
config_base="conf/"
config_app="system.conf"
config_log="logback.xml"

#::=== arguments

#::=== execute
#-Dconfig.resourse=$config_base/$config_app
nohup java -D$logger_name.configurationFile=$config_base/$config_log -jar $app_name >/dev/null 2>log &

