#!/usr/bin/env bash

sudo apk update
echo "=> install zuluJre-13.0.3"
	wget --quiet https://cdn.azul.com/public_keys/alpine-signing@azul.com-5d5dc44c.rsa.pub -P /etc/apk/keys/ && \
		echo "https://repos.azul.com/zulu/alpine" >> /etc/apk/repositories && \
	sudo apk --no-cache add zulu13-jre
echo "==> install libstdc++ and git"
	sudo apk add --no-cache libstdc++
	sudo apk add --no-cache git
echo "==> git clone repchain's deployTest environment"
	cd /home && sudo mkdir iscas && cd iscas
	sudo git clone https://gitee.com/BTAJL/QuicStart.git